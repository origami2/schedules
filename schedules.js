var debugs = {};

function Schedules (data, announce, fold) {
  this.data = data;
  this.announce = announce;
  this.debug = debugs[fold] || (debugs[fold] =
require('debug')('origami-stack:' + fold + ':schedules'));
}

Schedules.prototype.start = function () {
  var self = this;

  return self.data.find('schedules', {})
  .then(function (docs) {
    return new Promise(function (resolve, reject) {
      for (var i = 0; i < docs.length; i++) {
        var doc = docs[i];
      }
    });
  });
};

Schedules.prototype.stop = function () {

};

Schedules.prototype.list = function () {
  var self = this;

  return self.data.createCollection('schedules')
  .then(function () {
    return self.data.find('schedules', {})
    .then(function (docs) {
      return resolve(docs);
    }, reject);
  });
};

Schedules.prototype.getSchedule = function (_id) {
  var self = this;

  return self.data.createCollection('brainz-models')
  .then(function () {
    try {
      return self.data.findOne('schedules', { _id: { $oid: _id } })
      .then(function (doc) {
        if (!doc) return reject('Not found');

        return resolve(doc);
      });
    } catch (err) {
      reject(err);
    }
  }, reject);
};

Schedules.prototype.define = function (time, name, params) {
  var self = this;

  return self.data.createCollection('schedule')
  .then(function () {
     return self.data.insert(
       'brainz-models',
       {
         time: { $time: time },
         name: name,
         params: params
       });
  });
};

Schedules.prototype.undefine = function (_id) {
  var self = this;

  return self.data.remove(
    'schedules',
    {
      _id: { $oid: _id }
    }
  );
};

module.exports = Schedules;

var Plugin = require('origami2-api-plugin');

var plugin = new Plugin(require('./schedules'), {
  data: Plugin.require("Data")
});

if (module.parent === null) {
  plugin.connect('http://localhost:2000', function (err) {
    if (err) throw err;
  });
}
